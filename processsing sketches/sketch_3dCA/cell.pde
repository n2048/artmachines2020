class Cell {
  private boolean state;
  private float scale;
  private int val;  
  private color c;

  Cell (boolean state) {
    this.scale = random(.5,1.5);
    this.state = state;
    this.val = state ? 255 : 0;
    this.c = color(random(50,200),50);
  }

  void setState(boolean state) {
    this.state = state;
    this.val+= state ? 150 : -150;
    this.val = constrain(this.val,0,255);
  }

  boolean getState() {
    //return this.val > 127;
    return this.state;
  }

  float getScale() {
    return this.scale;
  }
  
  color getColor() {
    return this.c;
  }
}
