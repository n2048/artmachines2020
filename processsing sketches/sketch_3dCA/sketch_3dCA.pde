import oscP5.*;
import netP5.*;
import spout.*; 

Spout spout;
//PGraphics[] canvas;
//Spout[] receivers;

OscP5 oscP5;
NetAddress myRemoteLocation1;

int gridSize = 60;

int dim = 4;
int sSize = gridSize * dim;

float thd = .1;

Cell[][][] CA;

int z = 0;
PGraphics pgr; // Canvas to receive a texture
PGraphics pgca; // Canvas to draw the celular automata

void setup() {
  //fullScreen(P3D);
  //size(640, 480, P3D);
  size(1280, 720, P3D);
  //noCursor();

  oscP5 = new OscP5(this, 12003);
  myRemoteLocation1 = new NetAddress("127.0.0.1", 12001);

  spout = new Spout(this);
  //spout.createSender("ca", width, height);
  spout.createReceiver("depth");

  background(0);

  pgr = createGraphics(width, height, P2D);
  pgca = createGraphics(width, height, P3D);
  pgca.beginDraw();
  pgca.background(0);
  pgca.noStroke();
  pgca.endDraw();

  //receivers = new Spout[2];
  //receivers[0] = new Spout(this);   
  //receivers[0].createReceiver("depth");
  //receivers[1] = new Spout(this);   
  //receivers[1].createReceiver("grid");

  //canvas = new PGraphics[2];
  //canvas[0] = createGraphics(width, height, P2D);
  //canvas[1] = createGraphics(width, height, P2D);

  CA = new Cell[gridSize][gridSize][gridSize];
  init();
}

void init() {
  for (int i =0; i<gridSize; i++) {
    for (int j =0; j<gridSize; j++) {
      for (int k =0; k<gridSize; k++) {
        CA[i][j][k] = new Cell(false);
      }
    }
  }
}

void randomize() {
  for (int i =0; i<gridSize; i++) {
    for (int j =0; j<gridSize; j++) {
      CA[i][j][0] = new Cell(random(1)<.1);
    }
  }
}

void draw() {
  pgr = spout.receiveTexture(pgr);  

  pgca.beginDraw();
  pgca.camera(1.2*sSize, 1.2*sSize, 0.8*sSize, 
    sSize/2, sSize/2, sSize/2, 
    1, 1, 1);

  //camera(1.61*sSize, 1.6*sSize, 1.2*sSize, 
  //  sSize/2, sSize/2, sSize/2, 
  //  1, 1, 1);

  // this creates a translucid box for fade out
  showSimulationBox();

//  pgca.lights();
////  //pgca.directionalLight(255, 100, 100, cos(float(frameCount)/100.), sin(float(frameCount)/100.), -1);
//  pgca.directionalLight(50, 255, 30, -1, 1, -1);
////  //directionalLight(100, 100, 255, -1, -1, -1);
//  pgca.directionalLight(255, 100, 100, -1, -1, -1);
//  pgca.ambientLight(10, 10, 55);
////  //pgca.pointLight(51, 102, 126, 35, 40, 500);

  pgca.beginShape(TRIANGLE_STRIP);
  pgca.noFill();
  pgca.stroke(0, 14);
  for (int i =0; i<gridSize; i++) {
    for (int j =0; j<gridSize; j++) {
      for (int k =0; k<gridSize; k++) {
        if (CA[i][j][k].getState()) {
          pgca.vertex(i*dim+dim/2, j*dim+dim/2, k*dim+dim/2);
        }
      }
    }
  }
  pgca.endShape();
  pgca.noStroke();
  for (int i =0; i<gridSize; i++) {
    for (int j =0; j<gridSize; j++) {
      for (int k =0; k<gridSize; k++) {
        if (CA[i][j][k].getState()) {
          showBox(i, j, k);
          sendVoxel(i, j, k);
        }
      }
    }
  }

  update();

  pgca.endDraw();
  //canvas[0] = receivers[0].receiveTexture(canvas[0]);
  //canvas[1] = receivers[1].receiveTexture(canvas[1]);

  //image(canvas[0], 0, 0);
  //image(canvas[1], 0, 0);

  image(pgca,0,0);
  image(pgr,0,0,width/5,height/5);
  
  //spout.sendTexture();
  //reset();
}


void update() {

  z ++;
  // send current Layer
  if (z>=gridSize) {
    z=0;
    //reset();
    return;
  }

  int counter = 0;

  for (int i =0; i<gridSize; i++) {
    for (int j =0; j<gridSize; j++) {
      int c = getNeighborCount(i, j, z-1);
      if (CA[i][j][z-1].getState()) {
        if (c < 2) {
          CA[i][j][z].setState(false);
        } else if (c == 2) {
          CA[i][j][z].setState(true);
        } else if (c == 3) {
          CA[i][j][z].setState(true);
        } else if (c == 4) {
          CA[i][j][z].setState(false);
        } else {
          CA[i][j][z].setState(false);
        }
      } else {
        if (c == 3) {
          CA[i][j][z].setState(true);
        } else {
          CA[i][j][z].setState(false);
        }
      }
      counter += CA[i][j][z].getState()?1:0;
    }
  }
  z = z%gridSize;

  OscMessage myMessage = new OscMessage("/layer");
  myMessage.add(z);
  myMessage.add(counter);
  oscP5.send(myMessage, myRemoteLocation1);
}

int getNeighborCount(int i, int j, int k) {
  //println("i=", i, "j=", j, "k=", k);
  int count = 0;
  //MOORE neighbourhood
  if (CA[(i-1+gridSize)%gridSize][j][k].getState()) count ++; // left 
  if (CA[i][(j+1+gridSize)%gridSize][k].getState()) count ++; // top
  if (CA[i][(j-1+gridSize)%gridSize][k].getState()) count ++; // bottom
  if (CA[(i+1+gridSize)%gridSize][j][k].getState()) count ++; // right 
  //if (CA[(i-1+gridSize)%gridSize][(j+1+gridSize)%gridSize][k].getState()) count ++; // left top
  //if (CA[(i-1+gridSize)%gridSize][(j-1+gridSize)%gridSize][k].getState()) count ++; // left bottom
  //if (CA[(i+1+gridSize)%gridSize][(j+1+gridSize)%gridSize][k].getState()) count ++; // right top
  //if (CA[(i+1+gridSize)%gridSize][(j-1+gridSize)%gridSize][k].getState()) count ++; // right bottom
  return count;
}

void showBox(int x, int y, int z) {
  pgca.pushMatrix();
  pgca.translate(x*dim+dim/2, y*dim+dim/2, z*dim+dim/2);
  pgca.scale(dim, dim, dim);
  pgca.fill(CA[x][y][z].getColor());
  pgca.box(CA[x][y][z].getScale());  
  pgca.popMatrix();
}

void sendVoxel(int x, int y, int z) {
  int[] bang = {x, y, x};
  OscMessage myMessage = new OscMessage("/voxel");
  myMessage.add(bang);
  oscP5.send(myMessage, myRemoteLocation1);
}

void showSimulationBox() {
  pgca.pushMatrix();
  pgca.translate(sSize/2, sSize/2, sSize/2);
  pgca.scale(sSize, sSize, sSize);
  pgca.noStroke();
  pgca.strokeWeight(0.9/sSize);
  pgca.fill(0, 60);
  pgca.hint(DISABLE_DEPTH_TEST);
  pgca.box(1, 1, 1);
  pgca.popMatrix();
}


void oscEvent(OscMessage theOscMessage) { 
  for (int n = 1; n < gridSize-1; n++) {
    if (theOscMessage.checkAddrPattern("/grid/"+n)) {
      int l = theOscMessage.typetag().length();
      if (l != gridSize) {
        println("ERROR: dimension of the OSC stram should be ", gridSize);
      } else {

        for (int i = 1; i < gridSize-1; i++) {
          int v = theOscMessage.get(i).intValue();
          //println(v);
          CA[i][n][0].setState(v==1);

          CA[i][(n+1+gridSize)%gridSize][0].setState(v*random(1)>thd);
          CA[i][(n-1+gridSize)%gridSize][0].setState(v*random(1)>thd);
          CA[(i-1+gridSize)%gridSize][n][0].setState(v*random(1)>thd);
          CA[(i+1+gridSize)%gridSize][n][0].setState(v*random(1)>thd);
          
          //CA[(i-1+gridSize)%gridSize][(n+1+gridSize)%gridSize][0].setState(v*random(1)>thd);
          //CA[(i-1+gridSize)%gridSize][(n-1+gridSize)%gridSize][0].setState(v*random(1)>thd);
          //CA[(i+1+gridSize)%gridSize][(n+1+gridSize)%gridSize][0].setState(v*random(1)>thd);
          //CA[(i+1+gridSize)%gridSize][(n-1+gridSize)%gridSize][0].setState(v*random(1)>thd);
        }
      }
    }
  }
}
