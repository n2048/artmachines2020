import ch.bildspur.realsense.*;
RealSenseCamera camera = new RealSenseCamera(this);
import ch.bildspur.realsense.type.*;

import oscP5.*;
import netP5.*;

OscP5 oscP5;
NetAddress myRemoteLocation;

int maxDistance = 4;
int minDistance = 1;

int dim = 60;

void setup()
{
  size(640, 480, P3D);

  // width, height, fps, depth-stream, color-stream

  // enable depth stream
  camera.enableDepthStream(640, 480);
  
  // enable colorizer to display depth
  camera.enableColorizer(ColorScheme.Cold);
  
  
  // add threshold filter (limit distance)
  camera.addThresholdFilter(minDistance, maxDistance);
  camera.start();

  oscP5 = new OscP5(this, 12000);
  //myRemoteLocation = new NetAddress("127.0.0.1", 12001);
  myRemoteLocation = new NetAddress("192.168.86.20", 12001);
  
}

void draw()
{
  background(0);  

  // read frames
  camera.readFrames();


  PImage img = camera.getDepthImage();

  // show image
  //image(img, 0, 0);
  image(img, 
    0, 0, width, height, 
    0, 0, width, 1
    );

  loadPixels();

  stroke(255);
  fill(201,147,152,200);
  int w = width/dim;
  int h = height/dim;


  int[] radar = new int[dim];

  for (int i = 0; i < dim; i++) {
    line(0, i*h, width, i*h);
    line(i*w, 0, i*w, height);

    int x = i*w;
    float d = red(get().pixels[x+w/2+ width]);
    int z = floor(d*dim/255);
    radar[i] = z;

    rect(x, z*h, w, h);
  }
  sendGridData(radar);
}


void sendGridData(int[] grid) {
  /* in the following different ways of creating osc messages are shown by example */
  OscMessage myMessage = new OscMessage("/layer1");

  myMessage.add(grid);

  /* send the message */
  oscP5.send(myMessage, myRemoteLocation);
}
